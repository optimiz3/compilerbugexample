﻿using System;

using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace CompilerBugExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private const long ExpectedTicks =
            TimeSpan.TicksPerMinute * 5;

        private static readonly TimeSpan TestInterval =
            new TimeSpan(ExpectedTicks);

        public MainPage()
        {
            this.InitializeComponent();

            this.Loaded += MainPage_Loaded;
        }

        private async void MainPage_Loaded(
            object sender,
            RoutedEventArgs e)
        {
            var dialog = new MessageDialog(
                "Expected: " +
                ExpectedTicks +
                " Actual: " +
                TestInterval.Ticks);

            await dialog.ShowAsync();
        }
    }
}
